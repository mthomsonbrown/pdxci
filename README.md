I've been thinking about starting a hobby project, but was on the fence about the architecture. The app idea I have is a consigment goods listing for local businesses. So, I could shop on the website, and then perhaps have the item put on layaway, or just go and hope it's still there.

Seems fun, wouldn't cost the businesses anything except potentially time, and would support local business!

I got a hit from a recruiter today for a job converting a Ruby application to golang/typescript, which sounds like a fun stack, so I'm going to start honing my skills in those languages with this project!

...

!
